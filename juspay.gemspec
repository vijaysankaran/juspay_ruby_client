Gem::Specification.new do |s|
  s.name        = 'juspay'
  s.version     = '0.0.1'
  s.date        = '2013-05-15'
  s.summary     = "Juspay payment and card operations:"
  s.description = "Juspay payment and card operations"
  s.authors     = ["Vijay Sankaran"]
  s.email       = 'ninjas@juspay.in'
  s.files       = ["lib/juspay.rb"]
  s.homepage    =
    'http://rubygems.org/gems/juspay'
end
