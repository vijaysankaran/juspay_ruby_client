require 'rest_client'
require 'json'

class JuspayService
    def self.makeCall(url, params)
        base = JuspayConfiguration.environment
        key = JuspayConfiguration.key
        timeout = JuspayConfiguration.timeout
        open_timeout = JuspayConfiguration.open_timeout
        begin
            request = RestClient::Request.new( :method => :post, :url => base+url, :user => key, :payload => params, :timeout => timeout, :open_timeout => open_timeout )
            JSON.parse(request.execute)
            rescue Exception => e
                raise "Error when contacting Juspay: " + $!.inspect 
            end
        end
    
    def self.listCards(params) 
        self.makeCall("/card/list", params)
        end

    def self.addCard(params)
        self.makeCall("/card/add", params)
        end

    def self.deleteCard(params)
        self.makeCall("/card/delete", params)
        end

    def self.createOrder(params)
        self.makeCall("/init_order", params)
        end                     

    def self.makePayment(params)
        self.makeCall("/payment/handlePayV2", params)
        end
 
    def self.getOrderStatus(params)
        self.makeCall("/order_status", params)
        end
end

