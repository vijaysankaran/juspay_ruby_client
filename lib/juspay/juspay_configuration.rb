class JuspayConfiguration
    @@environment = JuspayEnvironment.production
    @@key = nil
    @@timeout = 30 # seconds
    @@open_timeout = 30 # seconds

    def self.init(key) 
        @@key = key
        end

    def self.environment
        return @@environment
        end
    def self.environment=(environment)
        @@environment = environment
        end
    def self.key
        return @@key
        end
    def self.key=(key)
        @@key = key
        end
    def self.timeout
        return @@timeout
        end
    def self.timeout=(timeout)
        @@timeout = timeout
        end
    def self.open_timeout
        return @@open_timeout
        end
    def self.open_timeout=(open_timeout)
        @@open_timeout = open_timeout
        end
end 
