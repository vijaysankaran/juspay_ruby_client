class JuspayEnvironment
    @@production = 'https://api.juspay.in'
    @@sandbox = 'https://sandbox.juspay.in'
    @@test = 'http://localapi.juspay.in'

    def self.production()
        return @@production
        end
    def self.sandbox()
        return @@sandbox
        end
    def self.test()
        return @@test
        end
end 
