require_relative 'lib/juspay'
require 'time'

# Note: Change the prodKey to what is provided to you

# JuspayConfiguration.init is better put in the scope of static execution

# Test
JuspayConfiguration.environment = JuspayEnvironment.test  
testKey = 'F5B57BB40FBF4F7C9D8AEE3B1D2E11F0' 
JuspayConfiguration.init(testKey)

# Prod
#JuspayConfiguration.environment = JuspayEnvironment.production  # Comment this line when testing 'test'
prodKey = '782CB4B3F5B84BDDB3C9EAFA6A134DC3'
#JuspayConfiguration.init(prodKey)  # Comment this line when testing 'test'

result = JuspayService.addCard({'customer_id'=>'guest_user',
    'customer_email'=>'guest@email.com',
    'card_number'=>1234561671216122,
    'card_exp_year'=>2014,
    'card_exp_month'=>05,
    'name_on_card'=>'Guest User',
    'nick_name'=>'Guest User'
})
p result
p JuspayService.listCards({'customer_id' => 'guest_user'})
p JuspayService.deleteCard({'card_token' => result['card_token']})

result = JuspayService.createOrder({
    'amount'=>100.0,
    'customer_id'=>'guest_user',
    'customer_email'=>'guest@email.com',
    'order_id'=>Time.now.to_i #random number
})
p result

p JuspayService.getOrderStatus({
    'order_id'=>result['order_id']
})

p JuspayService.makePayment({
    'merchant_id'=>'guest_merchant',
    'orderId'=>result['order_id'],
    'card_number'=>411122223333444412341234,
    'cardExpYear'=>'2020',
    'cardExpMonth'=>'05',
    'card_security_code'=>'111'
})

p JuspayService.getOrderStatus({
    'order_id'=>result['order_id']
})

